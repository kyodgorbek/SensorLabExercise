package com.example.sensorlabexercise;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements LocationManager {
    private static final String SHARED_PREF_NAME = "last_loc";
    private static final String PREF_LAST_LOCATION_LAT = "last_loc_cat";
    private static final String PREF_LAST_LOCATION_LONG = "last_loc_long";
    private static final String DEFAULT_VALUE = "0";
    private  static int MIN_TIME = 20000;
    private final static int MIN_TIME = 20000;
    private final static int MIN_DIST = 500;

    private Location mLastLocation;
    private SharedPreferences mPrefLocation;
    private LocationManager mLocationManager;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPrefLocation = getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        if (mPrefLocation.contains(PREF_LAST_LOCATION_LAT))&&
        mPrefLocation.contains(PREF_LAST_LOCATION_LONG) {
            Double lastLat = Double.parseDouble(mPrefLocation.getString(PREF_LAST_LOCATION_LAT,
                    DEFAULT_VALUE));
            setLocation(lastLat, lastLong);
        }
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mPrefLocation.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME,
                MIN_DIST, this);
        ((Button) findViewById(R.id.btnShowLast)).setOnClickListener((v) -> {
            toastLast(v.getContext());
        });

    }

    private void toastLast(Context context) {
        if(mLastLocation != null && context != null){
            Toast.makeText(context, "Last location lat:" + mLastLocation.getLatitude() +
                   "long:" + mLastLocation.getLongitude(), Toast.LENGTH_LONG.show();
        }

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
     setLocation(location.getLatitude(), location.getLongitude());
     toastLast(MainActivity.this);

    }

   @Override
   public void onStatusChanged(String provider, int status, Bundle extras) {


   }

   @Override
   public void onProviderEnabled(String provider) {

   }

   @Override
   public void setLocation(Double lastLat, Double lastLong) {
       if (mLastLocation == null) {
           mLastLocation = new Location(LocationManager.PASSIVE_PROVIDER);
       }
       mLastLocation.setLatitude(lastLat);
       mLastLocation.setLongitude(lastLong);
       SharedPreferences.Editor editor = mPrefLocation.edit();
       editor.putString(PREF_LAST_LOCATION_LAT, String.valueOf(lastLat));
       editor.putString(PREF_LAST_LOCATION_LONG, String.valueOf(lastLong));
       editor.commit();
   }
}


